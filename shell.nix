with import <nixpkgs> {};
mkShell {
  buildInputs = [
    go
    pkg-config
    alsaLib
    arduino-cli
    arduino
  ];
  shellHook = ''
    #newgrp dialout
  '';
}


