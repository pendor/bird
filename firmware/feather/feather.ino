#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
//#include <WiFi.h>

const int BATTERY_INTERVAL =      3000;
const int BUTTON_PIN =            13; //red emergency button
//const int BUTTON_PIN =            14;
const int LED_PIN =               0;
const int LED_BLINK_HALF_CYCLE =  100;

int sn;

void setup() {
  Serial.setTimeout(10);
  Serial.begin(9600);
  EEPROM.begin(512);
  wifi_setup();
  tcp_setup();
  //udp_setup();
  sn = ee_sn();
  button_setup();
  led_setup();
}

void loop() {
  ser_read();
  wifi();
  tcp();
  battery();
  button();
  led();
  delay(10);
}

////////////////////////////////////
//
// EEPROM
//

#define EE_SN   0
#define EE_SSID 4
#define EE_PASS 36
#define EE_IP   68
#define EE_PORT 84

int ee_sn(){
  int sn;
  EEPROM.get(EE_SN, sn);
  return sn;
}

void ee_sn_set(int val){
  EEPROM.put(EE_SN, val);
}

int ee_port(){
  int sn;
  EEPROM.get(EE_PORT, sn);
  return sn;
}

void ee_port_set(int val){
  EEPROM.put(EE_PORT, val);
}

void ee_ssid(char *buf, int len){
  char val[32];
  if(len > 33)
    len = 33;
  EEPROM.get(EE_SSID, val);
  strncpy(buf, val, len-1);
  buf[len-1] = 0;
}

void ee_ssid_set(char *buf){
  char val[32];
  strncpy(val, buf, 32);
  EEPROM.put(EE_SSID, val);
}

void ee_pass(char *buf, int len){
  char val[32];
  if(len > 33)
    len = 33;
  EEPROM.get(EE_PASS, val);
  strncpy(buf, val, len-1);
  buf[len-1] = 0;
}

void ee_pass_set(char *buf){
  char val[32];
  strncpy(val, buf, 32);
  EEPROM.put(EE_PASS, val);
}

void ee_ip(char *buf, int len){
  char val[16];
  if(len > 17)
    len = 17;
  EEPROM.get(EE_IP, val);
  strncpy(buf, val, len-1);
  buf[len-1] = 0;
}

void ee_ip_set(char *buf){
  char val[16];
  strncpy(val, buf, 16);
  EEPROM.put(EE_IP, val);
}

void ee_commit(){
  EEPROM.commit();
}

//////////////////////////////////
//
// WIFI
//

int wifi_status = 1000;
/*
    WL_NO_SHIELD        = 255,
    WL_IDLE_STATUS      = 0,
    WL_NO_SSID_AVAIL    = 1,
    WL_SCAN_COMPLETED   = 2,
    WL_CONNECTED        = 3,
    WL_CONNECT_FAILED   = 4,
    WL_CONNECTION_LOST  = 5,
    WL_DISCONNECTED     = 6
*/

void wifi() {
  int status = WiFi.status();
  if(status != wifi_status){
    wifi_status = status;
    if(Serial){
      Serial.print("WiFi status:");
      Serial.println(WiFi.status());
    }
  }
}

void wifi_setup() {
  char ssid[33], pass[33];
  ee_ssid(ssid, 33);
  ee_pass(pass, 33);
  if(Serial) {
    Serial.print("ssid:");
    Serial.print(ssid);
    Serial.print(": pass:");
    Serial.print(pass);
    Serial.println(":");
  }
  WiFi.begin(ssid, pass); 
}

bool wifi_connected() {
  return wifi_status == WL_CONNECTED;
}

//////////////////////////////////
//
// TCP
//

char        tcp_ip[32];
int         tcp_port;
WiFiClient  tcp_client;
int         tcp_status; //0 - disconnected/trying; 1 - connected

void tcp_setup() {
  ee_ip(tcp_ip, 32);
  tcp_port = ee_port();
}

void tcp() {
  if(!tcp_client.connected()){
    if(tcp_status == 1){
      if(Serial)
        Serial.println("TCP lost!");
      tcp_status = 0;
    }
    if(wifi_connected()){
      if(tcp_client.connect(tcp_ip, tcp_port)){
        if(Serial)
          Serial.println("TCP connected!");
        tcp_cmdstr("hello", "world");
        tcp_status = 1;
      }
    }
  }
}

bool tcp_connected(){
  return tcp_client.connected();
}

void tcp_cmdstr(char *cmd, char *msg)
{
  char buf[200];
  sprintf(buf, "sn:%d %s:%s", sn, cmd, msg);
  tcp_send(buf);
}

void tcp_cmdint(char *cmd, int val)
{
  char buf[200];
  sprintf(buf, "sn:%d %s:%d", sn, cmd, val);
  tcp_send(buf);
}

void tcp_cmdunit(char *cmd, int val, char *unit)
{
  char buf[200];
  sprintf(buf, "sn:%d %s:%d%s", sn, cmd, val, unit);
  tcp_send(buf);
}

void tcp_send(const char *msg) {
  bool connd = tcp_connected();
  //if(Serial){
  //  Serial.print("TCP:");
  //  Serial.print(msg);
  //  if(!connd)
  //    Serial.print(" - TCP unavailable");
  //  Serial.print("\n");
  //}
  if(!connd)
    return;
  tcp_client.println(msg);
}

//////////////////////////////////
//
// UDP
//udp_send

char  udp_ip[32];
int   udp_port;

void udp_setup() {
  ee_ip(udp_ip, 32);
  udp_port = ee_port();
}

void udp_send(const char *msg) {
 WiFiUDP udp;
 udp.beginPacket(udp_ip, udp_port);
 udp.write(msg, strlen(msg) + 1);
 udp.endPacket();
}

//////////////////////////////////
//
// Serial
//

int ser_active = 0;
char ser_buf[256];
int ser_bufpos = 0;
void ser_read() {
  if(!Serial){
    ser_active = 0;
    ser_bufpos = 0;
    return;
  }
  if(ser_active == 0){
    ser_active = 1;
    Serial.println("Hello feather");
  }
  ser_bufpos += Serial.readBytes(ser_buf + ser_bufpos, 255 - ser_bufpos);
  ser_buf[ser_bufpos] = 0;
  char *nl = strchr(ser_buf, '\n');
  if(nl == 0)
    return;
  *nl = 0;
  ser_process();
  //move rest of the string to front
  ser_bufpos = strlen(nl + 1);
  memmove(ser_buf, nl + 1, ser_bufpos + 1);
  Serial.println(ser_buf);
}

void ser_process() {
  char *val = strchr(ser_buf, ':');
  if(val == 0){
    Serial.println("invalid command");    
    return;
  }
  *val = 0;
  val++;
  if(!strcmp(ser_buf, "echo")){
    Serial.print("ECHO:");
    Serial.println(val);
    return;
  }
  if(!strcmp(ser_buf, "sn")){
    int sn = ee_sn();
    Serial.print("serial:");
    Serial.println(sn);
  }
  if(!strcmp(ser_buf, "sn_set")){
    int sn;
    sscanf((const char *)val, "%d", &sn);
    Serial.print("set serial:");
    Serial.println(sn);
    ee_sn_set(sn);
  }
  if(!strcmp(ser_buf, "ssid")){
    char buf[200];
    ee_ssid(buf, 200);
    Serial.print("SSID:");
    Serial.println(buf);
  }
  if(!strcmp(ser_buf, "ssid_set")){
    Serial.print("set SSID:");
    Serial.println(val);
    ee_ssid_set(val);
  }
  if(!strcmp(ser_buf, "pass")){
    char buf[200];
    ee_pass(buf, 200);
    Serial.print("password:");
    Serial.println(buf);
  }
  if(!strcmp(ser_buf, "pass_set")){
    Serial.print("set password:");
    Serial.println(val);
    ee_pass_set(val);
  }
  if(!strcmp(ser_buf, "ip")){
    char buf[200];
    ee_ip(buf, 200);
    Serial.print("IP:");
    Serial.println(buf);
  }
  if(!strcmp(ser_buf, "ip_set")){
    Serial.print("set IP:");
    Serial.println(val);
    ee_ip_set(val);
  }
  if(!strcmp(ser_buf, "port")){
    int port = ee_port();
    Serial.print("port:");
    Serial.println(port);
  }
  if(!strcmp(ser_buf, "port_set")){
    int port;
    sscanf((const char *)val, "%d", &port);
    Serial.print("set port:");
    Serial.println(port);
    ee_port_set(port);
  }
  if(!strcmp(ser_buf, "commit")){
    if(!strcmp(val, "yes")){
      Serial.println("commiting values to flash");
      ee_commit();
    }else
      Serial.println("won't commit - need 'yes'");
  }
  if(!strcmp(ser_buf, "now")){
    Serial.print("millis:");
    Serial.println(millis());
  }
  if(!strcmp(ser_buf, "wifi")){
    Serial.print("wifi:");
    Serial.println(wifi_status);
  }
  if(!strcmp(ser_buf, "tcp")){
    Serial.print("tcp:");
    Serial.println(tcp_status);
  }
}

//////////////////////////////////
//
// Battery
//

int bty_tstamp = 0;
void battery() {
  if(bty_tstamp + BATTERY_INTERVAL > millis())
    return;
  bty_tstamp = millis();
  int value = analogRead(A0);
  int volt = map(value, 580, 774, 3140, 4200);
  tcp_cmdunit("bty", volt, "mV");
}

////////////////////////////////////
//
// Button
//

int btn_state = 0;

void button_setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
}

void button() {
  int val = digitalRead(BUTTON_PIN) == LOW ? 0 : 1;
  if(val == btn_state)
    return;
  btn_state = val;
  if(Serial){
    Serial.print("btn:");
    Serial.println(btn_state);
  }

  tcp_cmdint("btn", btn_state);
}

//////////////////////////////////
//
// LED
//

int led_last = 0;
int led_state = 0;

void led_setup() {
  pinMode(LED_PIN, OUTPUT);
}

void led() {
  if(!wifi_connected()){
    if(millis() > led_last + LED_BLINK_HALF_CYCLE){
      led_last = millis();
      led_state = ~led_state;
      digitalWrite(LED_PIN, led_state);
    }
  }else{
    digitalWrite(LED_PIN, btn_state ? 0 : 1);
  }
}
