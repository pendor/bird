# solder on

 * `EN -> switch -> GND` (red) - this disables the gun if switch is set
 * `BAT -> R1 -> ADC -> R2 -> GND` (purple) - reduce voltage of battery (4.2) to ADC compatible (1.0). R1 / R2 must be > 4.2. 1M / 220k = 4.5
 * `14 -> button -> GND` (green) - button to press, internal pullup
 * `GND -> GND` (yellow)