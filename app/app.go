package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
	"bufio"

	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

type Feather struct {
	sn       int
	buf      *beep.Buffer
	vol      float64
	bat      int
	seen     time.Time
	btn      bool
	armed    bool
	fired    time.Time
	cooldown time.Duration
}

const (
	battery = iota
	button  = iota
)

const (
	released = iota
	pressed  = iota
)

type Command struct {
	sn  int
	typ int
	val int
}

func main() {
	cmdchan := make(chan Command)
	go udp_recv(cmdchan)
	go tcp_accept(cmdchan)

	format := beep.Format{
		SampleRate:  beep.SampleRate(48000),
		NumChannels: 2,
		Precision:   2,
	}
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/40))
	mix := beep.Mixer{}
	speaker.Play(&mix)

	feathers := make(map[int]*Feather)

	fea1 := Feather{
		sn:       456789,
		buf:      nil,
		vol:      0.0,
		bat:      0,
		seen:     time.Now(),
		btn:      false,
		armed:    true,
		fired:    time.Now().Add(-10 * time.Minute),
		cooldown: 5000 * time.Millisecond,
	}
	fea1.loadsound("gunshot.mp3", format)
	feathers[fea1.sn] = &fea1

	/*fea2 := Feather{
		sn:       456789,
		buf:      nil,
		vol:      0.0,
		bat:      0,
		seen:     time.Now(),
		btn:      false,
		armed:    true,
		fired:    time.Now().Add(-10 * time.Minute),
		cooldown: 200 * time.Millisecond,
	}
	fea2.loadsound("gunshot.mp3", format)

	feathers[fea2.sn] = &fea2*/

	for {
		select {
		case cmd := <-cmdchan:
			fea, ok := feathers[cmd.sn]
			if ok == false {
				continue
			}
			if cmd.typ == battery {
				fea.bat = cmd.val
				fea.seen = time.Now()
			}
			if cmd.typ == button {
				if cmd.val == pressed {
					fea.btn = true
					cooling := fea.cooldown - time.Now().Sub(fea.fired)
					if fea.armed == true && cooling < 0 {
						snd := fea.player()
						mix.Add(snd)
						fea.fired = time.Now()
					}
				}
				if cmd.val == released {
					fea.btn = false
				}
				printstatus(feathers)
			}
		case <-time.After(1 * time.Second):
			printstatus(feathers)
		}
	}
}

func printstatus(feathers map[int]*Feather) {
	for _, fea := range feathers {
		fea.status()
		fmt.Printf("     ")
	}
	fmt.Printf("\n")
}

func (fea *Feather) loadsound(fname string, fmt beep.Format) {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	resample := beep.Resample(6, format.SampleRate, fmt.SampleRate, streamer)
	buffer := beep.NewBuffer(fmt)
	buffer.Append(resample)
	streamer.Close()
	fea.buf = buffer
}

func (fea *Feather) player() beep.Streamer {
	return &effects.Volume{
		Streamer: fea.buf.Streamer(0, fea.buf.Len()),
		Base:     2,
		Volume:   fea.vol,
		Silent:   false,
	}
}

func (fea *Feather) status() {
	fmt.Printf("(%d: ", fea.sn)
	if fea.btn {
		fmt.Printf("[PRESSED] ")
	} else {
		fmt.Printf("[releasd] ")
	}
	gone := time.Now().Sub(fea.seen)
	if gone > 15*time.Second {
		unit := "s"
		amount := int32(gone / time.Second)
		if gone > 60*time.Second {
			unit = "m"
			amount = int32(gone / time.Minute)
		}
		fmt.Printf("[% 4d%s] ", amount, unit)
	} else {
		fmt.Printf("[alive] ")
	}
	if fea.armed == true {
		fmt.Printf("[armed] ")
	} else {
		fmt.Printf("[ SAVE] ")
	}
	cooling := fea.cooldown - time.Now().Sub(fea.fired)
	if cooling > 0 {
		fmt.Printf("[% 4d] ", int32(cooling/time.Second))
	} else {
		fmt.Printf("[cool] ")
	}
	fmt.Printf("[%d.%02dmV])", fea.bat/1000, (fea.bat%1000)/10)
}

func tcp_accept(ch chan Command) {
	ln,err := net.Listen("tcp", ":6746")
	if err != nil {
		fmt.Printf("Error Listening %v\n", err)
		return
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Printf("accept failt - weird")
			continue
		}
		go tcp_receive(conn, ch)
	}
}

func tcp_receive(conn net.Conn, ch chan Command) {
	reader := bufio.NewReader(conn)
	for {
		msg, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Connection Lost")
			return
		}
		msg_process(ch, msg)
	}
}

func udp_recv(ch chan Command) {
	buf := make([]byte, 256)
	addr := net.UDPAddr{
		Port: 6745,
		IP:   net.ParseIP("0.0.0.0"),
	}
	ser, err := net.ListenUDP("udp", &addr)
	if err != nil {
		fmt.Printf("Error Listening%v\n", err)
		return
	}
	for {
		_, _, err := ser.ReadFromUDP(buf)
		if err != nil {
			fmt.Printf("error while receiving packet: %v", err)
			continue
		}
		msg_process(ch, string(buf))
	}
}

func msg_process(ch chan Command, data string) {
	parts := strings.Split(data, " ")
	//serial number
	vals := strings.Split(parts[0], ":")
	if vals[0] != "sn" {
		fmt.Printf("Error parsing packet")
		return
	}
	x, err := strconv.ParseInt(vals[1], 10, 0)
	if err != nil {
		fmt.Printf("Error parsing serial number %v", err)
		return
	}
	sn := int(x)
	//arg
	vals = strings.Split(parts[1], ":")
	param := vals[0]
	if param == "bty" {
		mv := strings.Index(vals[1], "mV")
		if mv == -1 {
			fmt.Printf("Error parsing voltage")
			return
		}
		num := vals[1][:mv]
		x, err := strconv.ParseInt(num, 10, 0)
		if err != nil {
			fmt.Printf("Error parsing voltage: %v", err)
			return
		}
		volt := int(x)
		//fmt.Printf("got packet from sn:%d battery:%d\n", sn, volt)
		ch <- Command{sn, battery, volt}
	}
	if param == "btn" {
		if vals[1][0:1] == "1" {
			//fmt.Printf("got packet from sn:%d button pressed\n", sn)
			ch <- Command{sn, button, pressed}
		} else {
			//fmt.Printf("got packet from sn:%d button released\n", sn)
			ch <- Command{sn, button, released}
		}
	}
}
